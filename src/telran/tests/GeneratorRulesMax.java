package telran.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import telran.exceptions.RangeException;
import telran.exceptions.RuleException;
import telran.numbers.DividerRule;
import telran.numbers.Generator;

class GeneratorRulesMax {
    DividerRule divider100000 = new DividerRule ( 100000 );
    int min = 0, max = Integer.MAX_VALUE, nNumbers = 1000000000;

    @Test
    void testGenerate ( ) {

        Generator generator = null;
        generator = new Generator ( min, max, divider100000 );
        int[] ar = generator.generate ( nNumbers );
        assertEquals ( nNumbers, ar.length );
        for (int num : ar) {
            assertTrue ( num % 100000 == 0 && num >= min && num <= max );
        }
        try {
            generator = new Generator ( max, min, divider100000 );
            fail ( "Expected IllegalArgumentException" );
        } catch ( IllegalArgumentException e ) {

        }
    }

    @Test
    void testDivider ( ) {
        try {
            divider100000.checkRule ( 100000, min, max );
        } catch ( Exception e ) {
//            fail ( "Unexpected Exception" );
        }
        try {
            divider100000.checkRule ( 100002, min, max );
            fail ( "Expected RuleException" );
        } catch ( RuleException e ) {
            assertEquals ( -2, e.getDelta () );
        }
        try {
            divider100000.checkRule ( 100002, 100001, max );
            fail ( "Expected RuleException" );
        } catch ( RuleException e ) {
            assertEquals ( 99998, e.getDelta () );
        }

        try {
            divider100000.checkRule ( 100002, 100001, 199999 );
            fail ( "Expected RangeException" );
        } catch ( RangeException e ) {


        } catch ( RuleException e ) {
            fail ( "Unexpected Exception" );
        }
    }
}