package telran.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import telran.exceptions.RangeException;
import telran.exceptions.RuleException;
import telran.numbers.DividerRule;
import telran.numbers.Generator;

class GeneratorRulesTests {
    DividerRule divider10 = new DividerRule(10);
    int min = 1, max = 10000, nNumbers = 100000;
    @Test
    void testGenerate() {

        Generator generator = null;
        generator = new Generator(min, max, divider10);
        int[] ar = generator.generate(nNumbers);
        assertEquals(nNumbers, ar.length);
        for(int num: ar) {
            assertTrue(num % 10 == 0 && num >= min && num <= max);
        }
        try {
            generator = new Generator(max, min, divider10);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {

        }
    }
    @Test
    void testDivider()  {
        try {
            divider10.checkRule(10, min, max);
        } catch(Exception e) {
            fail("Unexpected Exception");
        }
        try {
            divider10.checkRule(12, min, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-2, e.getDelta());
        }
        try {
            divider10.checkRule(12, 11, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(8, e.getDelta());
        }
        try {
            divider10.checkRule(12, 11, 19);
            fail("Expected RangeException");
        }catch (RangeException e) {


        } catch (RuleException e) {
            fail("Unexpected Exception");
        }

        try {
            divider10.checkRule(48, 40, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(2, e.getDelta());
        }

        try {
            divider10.checkRule(65, 40, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-5, e.getDelta());
        }

        try {
            divider10.checkRule(16, 10, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(4, e.getDelta());
        }

        try {
            divider10.checkRule(65, 10, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-5, e.getDelta());
        }

        try {
            divider10.checkRule(4, 10, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(6, e.getDelta());
        }

        try {
            divider10.checkRule(56, 10, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(4, e.getDelta());
        }

        try {
            divider10.checkRule(46, 0, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(4, e.getDelta());
        }

        try {
            divider10.checkRule(44, 0, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-4, e.getDelta());
        }

        try {
            divider10.checkRule(16, 16, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(4, e.getDelta());
        }

        try {
            divider10.checkRule(63, 16, 64);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-3, e.getDelta());
        }

        try {
            divider10.checkRule(35, 40, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(5, e.getDelta());
        }

        try {
            divider10.checkRule(65, 40, 60);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-5, e.getDelta());
        }

        try {
            divider10.checkRule(max, min, max);
        } catch(Exception e) {
            fail("Unexpected Exception");
        }

        try {
            divider10.checkRule(65, 70, 100);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(5, e.getDelta());
        }

        // Class tests

        try {
            divider10.checkRule(12, min, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-2, e.getDelta());
        }

        try {
            divider10.checkRule(12, 11, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(8, e.getDelta());
        }

        try {
            divider10.checkRule(-10, 11, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(30, e.getDelta());
        }

        try {
            divider10.checkRule(20, 10, 15);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(-10, e.getDelta());
        }

        try {
            divider10.checkRule(18, min, max);
            fail("Expected RuleException");
        }catch (RuleException e) {
            assertEquals(2, e.getDelta());
        }

    }

}
