package telran.numbers;

import telran.exceptions.RuleException;
import telran.exceptions.RangeException;
public class DividerRule implements Rule

{

    int divider;
    public DividerRule(int divider)

    {
        this.divider = divider;
    }


    @Override
    public void checkRule(int number, int min, int max) throws RuleException

    {

        int minAdapt = minAdapt (min);
        int maxAdapt = maxAdapt (max);

        if ((min > max)||(minAdapt > maxAdapt)) {
            throw new RangeException("Range Exception");
        }


        int delta = getDelta(number, minAdapt, maxAdapt);
        if (delta != 0) {
            throw new RuleException(delta);
        }

    }



    private int getDelta(int number, int min, int max)

    {
        if (number > max) return max - number;
        if (number < min) return min - number;

        int delta = number % divider;

        if (delta == 0) {
            return 0;
        }

        if ( divider - delta > delta  ) {
            if ( min <= number - delta ) return -delta;
            return (divider - delta);
        } else {
            if ( max >= number + divider - delta ) return (divider - delta);
            return -delta;
        }

    }

    private int minAdapt ( int number )

    {
        int remainder = number % divider;
        if (remainder == 0) return number;

        int res = number - remainder;
        res = res + divider;

        return res;
    }

    private int maxAdapt ( int number )

    {
        int remainder = number % divider;
        if (remainder == 0) return number;

        return number - remainder;
    }
}