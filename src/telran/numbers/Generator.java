package telran.numbers;

import telran.exceptions.RuleException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static java.lang.Math.random;

public class Generator {

    int min;
    int max;
    Rule rule;


    public Generator ( int min, int max, Rule rule ) {
        if ( max < min ) {
            throw new IllegalArgumentException ();
        }
        this.min  = min;
        this.max  = max;
        this.rule = rule;
    }


    public int[] generate ( int nNumbers ) {

        ArrayList<Integer> buffer = new ArrayList<> ();
        int number, i = 0;

        while (i < nNumbers) {
            number = (int) ( max * random () + min );
            try {
                rule.checkRule ( number, min, max );
                buffer.add ( number );
                i++;
            } catch ( RuleException e ) {
                buffer.add ( number + e.getDelta () );
                i++;
            }
        }

        return convertIntegers ( buffer );
    }

    private static int[] convertIntegers(ArrayList<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        Arrays.setAll ( ret, i -> iterator.next () );
        return ret;
    }

    public Rule getRule ( ) {
        return this.rule;
    }


    public void setRule ( Rule rule ) {
        this.rule = rule;
    }

}
